const SpotifyWebApi = require('spotify-web-api-node');
const helper = require('../misc/helperFunctions')
//Holds clientId and clientSecret
const auth = require('../auth.json');
//Used for testing
const fs = require('fs');

//Init Spotify API
let spotifyApi = new SpotifyWebApi({
    clientId: auth.clientID,
    clientSecret: auth.clientSecret,
});



/**
 * Main Spotify Function:
 * TODO: Probably clean up code duplication
 * TODO: FIX order in which music plays
 * @param spotifyLink
 * @param message
 * @param distube
 */
function playSpotify(spotifyLink, message, distube){
    const Discord = require('discord.js');

    //Error Embed
    const ErrorEmbed = new Discord.MessageEmbed().setColor("#ff0000");

    // get the access token
    spotifyApi.clientCredentialsGrant().then(
        function(data) {
            //save access token so that it can be used in future calls
            spotifyApi.setAccessToken(data.body['access_token']);
            const result = getSpotifyId(spotifyLink)
            const spotifyID = result[0];
            const foundID = result[1];
            const type = result[2];



            if(!foundID){
                message.channel.send(ErrorEmbed.setDescription("Sorry but nothing was found given your link")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                return;
            }

            switch (type) {
                case "Track":
                    spotifyApi.getTrack(spotifyID).then(function(data) {

                        //get track name and artist
                        let trackName;
                        let artist;

                        trackName = data.body.name;
                        artist = data.body.artists[0].name;

                        //Find youtube version of spotify song
                        let searchString = trackName + " " + artist;
                        distube.search(searchString).then(function(data){
                            let searchResult = data[0].url;
                            distube.play(message, searchResult);
                        },function(err){
                            message.channel.send(ErrorEmbed.setDescription("Sorry but something while finding your song on Youtube")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                            console.log('Something went wrong!', err);
                        });

                    }, function(err) {
                        message.channel.send(ErrorEmbed.setDescription("Sorry but something went wrong while using Spotify API")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                        console.log('Something went wrong!', err);
                    });


                break;
                case "Playlist":
                    let offset = 0;
                    let limit = 100
                    let totalTracks = 10;

                    let newID = spotifyID.split("?")[0];

                    //Find youtube links for each song in the spotify album
                    let arrayOfYoutubeLinks = [];

                    while(offset + limit < totalTracks){

                        spotifyApi.getPlaylistTracks(newID, { market: ["DK"], offset: offset, limit: limit})
                            .then(function(data) {

                                //get track name and artist
                                let nameOfTracks = [];
                                let artistOfTracks = [];

                                data.body.items.map(track => {
                                    nameOfTracks.push(track.track.name);
                                    artistOfTracks.push(track.track.artists[0])
                                });


                                const trackLoop = async _ => {
                                    let currentTrackIndex = 0;
                                    const link = nameOfTracks.map(async track => {
                                        let searchString = track + " " + artistOfTracks[currentTrackIndex].name;
                                        try{
                                            let searchResults = await distube.search(searchString);
                                            arrayOfYoutubeLinks.push(searchResults[0].url);
                                            currentTrackIndex++;
                                            return searchResults;
                                        } catch (err){
                                            console.log('Song couldn\'t be found', err);
                                        }
                                    });
                                    const testing = await Promise.all(link);
                                }

                                trackLoop().then(() => {
                                    totalTracks = data.body.total;
                                    offset = offset + 100;
                                });

                            }, function(err) {
                                message.channel.send(ErrorEmbed.setDescription("Sorry but something went wrong while using Spotify API")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                                console.log('Something went wrong!', err);
                            });
                    }
                    distube.playCustomPlaylist(message, arrayOfYoutubeLinks);

                break;
                case "Album": //TODO: Probably check if album is longer than 100 tracks
                    spotifyApi.getAlbum(spotifyID).then(function(data) {

                        //get track name and artist
                        let nameOfTracks = [];
                        let artistOfTracks = [];

                        data.body.tracks.items.map(track => {
                            nameOfTracks.push(track.name);
                            artistOfTracks.push(track.artists[0])
                        });

                        //Find youtube links for each song in the spotify album
                        let arrayOfYoutubeLinks = [];

                        const trackLoop = async _ => {
                            let currentTrackIndex = 0;
                            const link = nameOfTracks.map(async track => {
                                let searchString = track + " " + artistOfTracks[currentTrackIndex].name;
                                try{
                                    let searchResults = await distube.search(searchString);
                                    arrayOfYoutubeLinks.push(searchResults[0].url);
                                    currentTrackIndex++;
                                    return searchResults;
                                } catch (err){
                                    console.log('Song couldn\'t be found', err);
                                }
                            });
                            const testing = await Promise.all(link);
                        }

                        trackLoop().then(() => {
                            distube.playCustomPlaylist(message, arrayOfYoutubeLinks);
                        });


                    }, function(err) {
                        message.channel.send(ErrorEmbed.setDescription("Sorry but something went wrong while using Spotify API")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                        console.log('Something went wrong!', err);
                    });


                break;
                case "Artist":
                    spotifyApi.getArtist(spotifyID).then(function(data) {
                        message.channel.send("Found artist")

                        spotifyApi.getArtistTopTracks(data.body.id, "DK").then(function(data) {
                            //TEST CODE
                            let newData = JSON.stringify(data.body)
                            fs.writeFileSync("artist.json", newData);

                            //get track name and artist
                            let nameOfTracks = [];
                            let artistOfTracks = [];

                            data.body.tracks.map(track => {
                                nameOfTracks.push(track.name);
                                artistOfTracks.push(track.artists[0])
                            });

                            //Find youtube links for each song in the spotify album
                            let arrayOfYoutubeLinks = [];

                            const trackLoop = async _ => {
                                let currentTrackIndex = 0;
                                const link = nameOfTracks.map(async track => {
                                    let searchString = track + " " + artistOfTracks[currentTrackIndex].name;
                                    try{
                                        let searchResults = await distube.search(searchString);
                                        arrayOfYoutubeLinks.push(searchResults[0].url);
                                        currentTrackIndex++;
                                        return searchResults;
                                    } catch (err){
                                        console.log('Song couldn\'t be found', err);
                                    }
                                });
                                const testing = await Promise.all(link);
                            }

                            trackLoop().then(() => {
                                distube.playCustomPlaylist(message, arrayOfYoutubeLinks);
                            });

                        }, function(err){

                        });

                    }, function(err) {
                        message.channel.send(ErrorEmbed.setDescription("Sorry but something went wrong while using Spotify API")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                        console.log('Something went wrong!', err);
                    });
                break;
                case "Show"://USELESS CODE
                    spotifyApi.getShowEpisodes(spotifyID,{market: ["DK"]}).then(function(data) {

                        message.channel.send(ErrorEmbed.setDescription("So this isn't really implemented, since it is kinda useless")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));

                    }, function(err) {
                        message.channel.send(ErrorEmbed.setDescription("Sorry but something went wrong while using Spotify API")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                        console.log('Something went wrong!', err);
                    });
                break;
                case "Episode": //USELESS CODE

                    spotifyApi.getEpisode(spotifyID,{market: ["DK"],offset: 60}).then(function(data) {
                        //get track name and artist
                        let trackName;
                        let artist;

                        trackName = data.body.name;
                        artist = data.body.artists[0].name;

                        message.channel.send(ErrorEmbed.setDescription("So this isn't really implemented, since it is kinda useless")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
/*                      //Find youtube version of spotify song
                        let searchString = trackName + " " + artist;
                        distube.search(searchString).then(function(data){
                            let searchResult = data[0].url;
                            distube.play(message, searchResult);
                        },function(err){
                            message.channel.send(ErrorEmbed.setDescription("Sorry but something while finding your song on Youtube")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                            console.log('Something went wrong!', err);
                        });*/


                    }, function(err) {
                        message.channel.send(ErrorEmbed.setDescription("Sorry but something went wrong while using Spotify API")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                        console.log('Something went wrong!', err);
                    });
                break;
                default:
                    message.channel.send(ErrorEmbed.setDescription("Sorry but something major went wrong, please use command !feedback")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
                    break;
            }

        },
        function(err) {
            message.channel.send(ErrorEmbed.setDescription("Something went wrong when retrieving an access token")).then(newMSG => helper.messageDeleteAfterTime(newMSG,30000, message));
            console.log('Something went wrong when retrieving an access token', err);
        }
    );
}




/**
 * Get the Spotify id for either a track, artist, album, or playlist TODO: Add support for podcast
 */
function getSpotifyId(link){
    let spotifyId = "";
    let foundID = false;
    //Check if it is a track
    if (link.includes("open.spotify.com/track/")){
        spotifyId = link.split("track/")[1];
        if(spotifyId.length >= 1){
            foundID = true;
        }
        return [spotifyId, foundID, "Track"];
    }
    //Check if it is a playlist
    if (link.includes("open.spotify.com/playlist/")){
        spotifyId = link.split("playlist/")[1];
        if(spotifyId.length >= 1){
            foundID = true;
        }
        return [spotifyId, foundID, "Playlist"];
    }
    //Check if it is a album
    if (link.includes("open.spotify.com/album/")){
        spotifyId = link.split("album/")[1];
        if(spotifyId.length >= 1){
            foundID = true;
        }
        return [spotifyId, foundID, "Album"];
    }
    //Check if it is a artist
    if (link.includes("open.spotify.com/artist/")){
        spotifyId = link.split("artist/")[1];
        if(spotifyId.length >= 1){
            foundID = true;
        }
        return [spotifyId, foundID, "Artist"];
    }
    //Check if it is a show/podcast
    if (link.includes("open.spotify.com/show/")){
        spotifyId = link.split("show/")[1];
        if(spotifyId.length >= 1){
            foundID = true;
        }
        return [spotifyId, foundID, "Show"];
    }
    //Check if it is an episode for a show
    if (link.includes("open.spotify.com/episode/")){
        spotifyId = link.split("episode/")[1];
        if(spotifyId.length >= 1){
            foundID = true;
        }
        return [spotifyId, foundID, "Episode"];
    }
    return [spotifyId, foundID, "Error"];

}

module.exports = {playSpotify,getSpotifyId}