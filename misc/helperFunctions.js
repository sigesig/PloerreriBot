/**
 * Should Probably add some checks that the parameters are correct
 * @param currMessage
 * @param deleteTime in milli seconds
 * @param prevMessage
 */
function messageDeleteAfterTime(currMessage, deleteTime, prevMessage){
    prevMessage = prevMessage || 0;
    currMessage.delete({timeout: deleteTime})
    if(prevMessage !== 0){
        prevMessage.delete({timeout: deleteTime})
    }
}

function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < milliseconds);
}

function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}

module.exports = {messageDeleteAfterTime, sleep, validURL}