const spotify = require('../misc/spotify');
/**
 * Test List:
 * 1.   Get token successfully
 * 2.   Get spotify ID
 */


test('get track id, link as string', () => {
    const link = "https://open.spotify.com/track/0A4PZuepTcIQVvA5m7R0M1?si=fknryrBoQAiHQlvp6FPW_w";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["0A4PZuepTcIQVvA5m7R0M1?si=fknryrBoQAiHQlvp6FPW_w", true, "Track"])
});

test('get playlist id, link as string', () => {
    const link = "https://open.spotify.com/playlist/2s9fzDFDFcn7qZg7oBYfMc?si=_TJB9gpOR_qg4PDn__c_Tw";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["2s9fzDFDFcn7qZg7oBYfMc?si=_TJB9gpOR_qg4PDn__c_Tw", true, "Playlist"])
});
test('get album id, link as string', () => {
    const link = "https://open.spotify.com/album/2s9fzDFDFcn7qZg7oBYfMc?si=_TJB9gpOR_qg4PDn__c_Tw";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["2s9fzDFDFcn7qZg7oBYfMc?si=_TJB9gpOR_qg4PDn__c_Tw", true, "Album"])
});

test('get artist id, link as string', () => {
    const link = "https://open.spotify.com/artist/2s9fzDFDFcn7qZg7oBYfMc?si=_TJB9gpOR_qg4PDn__c_Tw";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["2s9fzDFDFcn7qZg7oBYfMc?si=_TJB9gpOR_qg4PDn__c_Tw", true, "Artist"])
});

test('get show id, link as string', () => {
    const link = "https://open.spotify.com/show/78qsJtKK124FRozcWmwBJb?si=EgxnmhRfT9WQ6RrHc5hkMg";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["78qsJtKK124FRozcWmwBJb?si=EgxnmhRfT9WQ6RrHc5hkMg", true, "Show"])
});

test('get episode of show id, link as string', () => {
    const link = "https://open.spotify.com/episode/40obdtNWtmUNo1SYW8a1wU?si=aBNiWtU7SQ26O08qJwxJpg";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["40obdtNWtmUNo1SYW8a1wU?si=aBNiWtU7SQ26O08qJwxJpg", true, "Episode"])
});

test('wrong format link, link as string', () => {
    const link = "https://open.spotify.com/playlisst/2s9fzDFDFcn7qZg7oBYfMc?si=_TJB9gpOR_qg4PDn__c_Tw";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["", false, "Error"])
});

test('no spotifyID found', () => {
    const link = "https://open.spotify.com/playlist/";
    expect(spotify.getSpotifyId(link)).toStrictEqual(["", false, "Playlist"])
});


