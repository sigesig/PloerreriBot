const fs = require('fs')
const Discord = require("discord.js");
//TODO: Finish Command
module.exports = {
    name: 'druk',
    description: "Drinking rules for different games",
    indepth:"",
    usage: "Just type !druk **game** \n i.e. !druk lol",
    execute(message, args){
            const embedErrorReadingJSON = new Discord.MessageEmbed().setDescription("Sorry an error happened while reading the rules").setColor("#ff0000");
        if(!args.join()){

            //Read the drinkingrules.json
            fs.readFile("./drinkingrules.json", 'utf8', (err, jsonString) => {
                if (err) {
                    console.log("Error reading file from disk:", err)
                    message.channel.send(embedErrorReadingJSON);
                    return
                }
                try {
                    const allRules = JSON.parse(jsonString)
                    const drukHelpEmbed = new Discord.MessageEmbed().setTitle("Druk regler").setColor("#0099ff").setDescription("To get the rules for a specific game type \"!druk **The game**\" remember upper case(cause i'm to lazy to fix it)")
                        .addField("Currently supported games:", "•   " +Object.keys(allRules).join("\n •   "));
                    message.channel.send(drukHelpEmbed).then(message => message.delete({timeout: 90000}));
                } catch(err) {
                    console.log('Error parsing JSON string:', err)
                    message.channel.send(embedErrorReadingJSON).then(message => message.delete({timeout: 10000}));
                }
            })

        } else {
            fs.readFile('./drinkingrules.json', 'utf-8', function (err, data) {
                if (err) {
                    console.error(err)
                    message.channel.send(embedErrorReadingJSON);
                    return
                }
                try{
                    const Embed = new Discord.MessageEmbed().setColor("#0099ff");
                    let json = JSON.parse(data);
                    const gamesSupported = Object.keys(json)

                    //Check if the game is known
                    if(gamesSupported.includes(args.join(' '))){
                        let gameRules = json[args.join(' ')];

                        Embed.setTitle(args.join(' '));
                        let rules = "";
                        Object.entries(gameRules).forEach(([key, value]) => {
                            rules = rules + key + ":   " + value + "\n"
                        })
                        Embed.setDescription(rules);
                        message.channel.send(Embed);
                    } else {
                        embedErrorReadingJSON.setDescription("I don't know that game");
                        message.channel.send(embedErrorReadingJSON).then(message => message.delete({timeout: 10000}));
                    }

                } catch (err){
                    console.log('Error parsing JSON string:', err)
                    message.channel.send(embedErrorReadingJSON).then(message => message.delete({timeout: 10000}));
                }


            });
        }




    }
}