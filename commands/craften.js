const Discord = require('discord.js')
const serverInfo = require('../minecraftServer.json')
const helper = require('../misc/helperFunctions')

//TODO: Not done yet, basics done
module.exports = {
    name: 'craften',
    description: "Information about the current Minecraft server",
    indepth:"General information about the current Minecraft Server. Information can be changed using either IP, Map, Version, ",
    usage: "Just type !craften",
    execute(message, args){
        if(!args[0]){
            message.channel.send(createEmbed()).then(newMSG => helper.messageDeleteAfterTime(newMSG,90000, message));
            return;
        }

        if (args[0] === "add"){
            return;
        }
        if (args[0] === "edit"){
            return;
        }
        if (args[0] === "delete"){
            return;
        }

    function createEmbed(){
        const Embed = new Discord.MessageEmbed().setTitle("Craften")
            .setColor("#0099ff")
            .setThumbnail("https://icons.iconarchive.com/icons/papirus-team/papirus-apps/512/minecraft-icon.png");

        Object.entries(serverInfo).forEach(([key, value]) => {
            Embed.addField(key, value)
        });

        return Embed;
    }

    }
}