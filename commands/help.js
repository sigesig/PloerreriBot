const Discord = require("discord.js");
module.exports = {
    name: 'help',
    description: "List of commands or specific command(!help **command**)",
    indepth:"",
    usage: "Just type !Help to get a list of all commands \n For help on a specific command type !help **command**",
    //TODO: Fix that arrow back is not always shown same as arrow forward
    execute(message, args, bot){
        const emojiNext = '➡'; // unicode emoji are identified by the emoji itself
        const emojiPrevious = '⬅';
        const reactionArrow = [emojiPrevious, emojiNext];
        let currentPage = 0;
        const time = 60000; // time limit: 1 min
        let lastCommand = "yousef"; // Shit solution

        let commandsPageList = [];
        const lengthOfCommandLists = 8;

        //Make array of list for commands to be easy switched between
        if (args[0] == null ){
            let commandsPage = [];
            let currentNumberOfCommandsAdded = 0;
            let commandsPageListTracker = 0;
            bot.commands.forEach(command => {
                if(currentNumberOfCommandsAdded < lengthOfCommandLists && !(command.name === lastCommand)){
                    commandsPage[currentNumberOfCommandsAdded] = command;
                    currentNumberOfCommandsAdded++;
                } else if(command.name === lastCommand){
                    commandsPageList[commandsPageListTracker] = commandsPage;
                    commandsPageListTracker++;
                    commandsPage = [];
                    currentNumberOfCommandsAdded = 0;
                    commandsPage[currentNumberOfCommandsAdded] = command;
                    currentNumberOfCommandsAdded++;
                } else{
                    //Add to master array and reset small array
                    commandsPageList[commandsPageListTracker] = commandsPage;
                    commandsPageListTracker++;
                    commandsPage = [];
                    currentNumberOfCommandsAdded = 0;
                    //add to small array
                    commandsPage[currentNumberOfCommandsAdded] = command;
                    currentNumberOfCommandsAdded++;
                }
            });

            function filter(reaction, user){
                // check if the emoji is inside the list of emojis, if the user is not a bot, and if the reactor is the person who typed help
                return (!user.bot) && (reactionArrow.includes(reaction.emoji.name)) && (user.id === message.author.id);
            }

            //Create an embed for command list at page number
            function createlist(commandsList, pageNr){
                const Embed = new Discord.MessageEmbed()
                    .setTitle('List of commands:')
                    .setColor('#0099ff');

                const list = commandsList[pageNr];
                list.forEach(command => Embed.addField('!' + command.name, command.description));
                Embed.setFooter("Page " +(pageNr+1) + "/" + commandsPageList.length)
                return Embed;
            }
            //Start reaction controller and first message
            const firstListEmbed = createlist(commandsPageList, currentPage)
            message.channel.send(firstListEmbed).then(newMSG => {
                newMSG.react(emojiPrevious);
                newMSG.react(emojiNext).then(msgReact => {
                    let i = 0;
                    const collector = msgReact.message.createReactionCollector(filter, { time });
                    collector.on('collect', r => {
                        if ((r.emoji.name === emojiPrevious) && (i > 0)) {
                            currentPage--;
                            msgReact.message.edit(createlist(commandsPageList,currentPage));
                            r.users.remove(message.author);
                            i--;
                        } else if ((r.emoji.name === emojiNext) && (i < commandsPageList.length-1)) {
                            currentPage++
                            msgReact.message.edit(createlist(commandsPageList,currentPage));
                            r.users.remove(message.author);
                            /*msgReact.message.reactions.removeAll().then(() =>{
                                if (currentPage < commandsPageList.length-1){
                                    msgReact.message.react(emojiPrevious);
                                    msgReact.message.react(emojiNext);
                                } else {
                                    msgReact.message.react(emojiPrevious);
                                }
                            });*/
                            i++;
                        }
                    });
                    collector.on('end', collected => {
                        msgReact.message.delete();
                        message.delete();
                    });
                });
                console.log(commandsPageList.length);
            });




        } else{ // If an arguments is after its to call help on a specific command
            const Embed = new Discord.MessageEmbed()
                .setTitle('List of commands')
                .setColor('#0099ff');

            let commandExists = false;
            let specificCommand = args[0];
            bot.commands.forEach(commandsPageList => {
                    if((specificCommand === commandsPageList.name)) {
                        if(commandExists) return;
                        commandExists = true;
                        specificCommand = bot.commands.get(args[0]);
                    }
                }
            )
            if (!commandExists){
                const ErrorEmbed = new Discord.MessageEmbed().setDescription("Sorry, but i dont know that commandsPageList you are looking for :>").setColor("#ff0000");
                message.channel.send(ErrorEmbed);
                return;
            }

            const title = specificCommand.name.charAt(0).toUpperCase() + specificCommand.name.slice(1);

            if (specificCommand.indepth.length > 0){
                Embed.setTitle(title).addField('Description', specificCommand.indepth).addField("Usage", specificCommand.usage);
            }else{
                Embed.setTitle(title).addField('Description', specificCommand.description).addField("Usage", specificCommand.usage);
            }
            message.channel.send(Embed);
        }

    }
}
