const Discord = require('discord.js');

module.exports = {
    name: 'autoplay',
    description: "Changes autoplay for the bot, this is the autoplay from Youtube",
    indepth:"Changes autoplay for the bot, this is the autoplay from Youtube. So it will continue playing songs from Youtube even though no more are in queue",
    usage: "Just type !autoplay",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to change if it autoplays music").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                let mode = distube.toggleAutoplay(message);
                const Embed = new Discord.MessageEmbed().setDescription("Set autoplay mode to `" + (mode ? "On" : "Off") + "`").setColor("#FFFF00");
                message.channel.send(Embed);
                message.react(emoji);
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}