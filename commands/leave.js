const Discord = require('discord.js');

module.exports = {
    name: 'leave',
    description: "Makes the bot leave the voice channel",
    indepth:"",
    usage: "Just type !leave",
    execute(message, args){
        let emoji = '👋';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to make it leave").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                //distube.stop(message); commented out because it has been moved to main
                message.guild.voice.channel.leave();
                message.react(emoji);
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}