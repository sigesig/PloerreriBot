const fs = require('fs');
const Discord = require('discord.js');

function reactToMessage(message) {
    message.react("✅");
    message.react("❌");
}

module.exports = {
    name: 'idea',
    description: "Used to suggest a feature to be added to the bot",
    indepth:"Used to suggest a feature to be added to the bot. This can be anything, whatever you think could be nice just suggest that.",
    usage: "Just type !idea **the idea**",
    execute(message, args){
        if(args.length < 1){
            const errorEmbed = new Discord.MessageEmbed().setDescription("You actually need to give an idea :>").setColor("#FF0000")
            message.channel.send(errorEmbed);
            return;
        }
        const ideaEmbed = new Discord.MessageEmbed()
            .setTitle("Idea")
            .setAuthor(message.author.username,message.author.avatarURL())
            .setTimestamp()
            .setDescription(args.join(' '),)
            .setColor("#ffff00")
            .setFooter(message.guild.name);
        message.client.users.cache.get('256807585804582912').send(ideaEmbed).then(newMSG =>
            reactToMessage(newMSG)
        )
        const Embed = new Discord.MessageEmbed().setDescription("Your idea has been received, Thanks!").setColor("#0099ff")
        message.channel.send(Embed);
    }
}