const Discord = require('discord.js');
const helper = require('../misc/helperFunctions')

module.exports = {
    name: 'jump',
    description: "Makes the bot go to a specific song in the queue",
    indepth:"",
    usage: "Just type !jump",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to skip to the next song").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000, message));
        }else {
            if (inChannel === message.guild.voice.channel) {
                if(distube.isPlaying(message)) {
                    if(args[0].length > 0){
                        if(parseInt(args[0]) < 1){
                            message.channel.send(EmbedError.setDescription("Fuck dig Christoffer jeg har tjekket det :>\n Så giv lige et nummer som giver mening")).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000, message));
                            return;
                        }
                        if(parseInt(args[0]) > distube.getQueue(message).songs.length){
                            message.channel.send(EmbedError.setDescription("How about giving some proper value that is in the scope")).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000, message));
                            return;
                        }
                        distube.jump(message, parseInt(args[0])-1);
                    } else {
                        const noArgEmbed = new Discord.MessageEmbed().setDescription("I need a point to jump to in the queue").setColor("#ff0000");
                        message.channel.send(noArgEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000, message));
                    }
                }else {
                    const notPlayingEmbed = new Discord.MessageEmbed().setDescription("I'm currently not playing anything").setColor("#ff0000");
                    message.channel.send(notPlayingEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000, message));
                }
            } else {
                message.channel.send(EmbedError).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000, message));
            }

        }
    }
}