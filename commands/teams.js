const Discord = require('discord.js')
module.exports = {
    name: 'teams',
    description: "This is a command to create randomized teams",
    indepth:"his is a command to create randomized teams",
    usage: "Just type !teams",
    execute(message){

        //TODO: Change make possible to move people to voice chats

        //Time Limit
        const time = 28800000; // time limit: 1 min
        //Message deleted
        let messageDeleted = false;

        //Reactions variables
        const emojiJoin = '✅';
        const emojiLeave = '❌';
        const emojiIncrement = '⬆️';
        const emojiDecrement = '⬇️';
        const emojiOk = '🆗';
        const emojiStop = '🛑';
        const emojiShuffle = '🔀';
        const participantsReactions = [emojiJoin, emojiLeave];

        //Controls which reactions only the creator can press
        const creatorReactions = [emojiDecrement, emojiIncrement, emojiOk, emojiStop, emojiShuffle];

        //keeping track of the current participants
        let mapOfParticipants = new Map();

        //Add creator self to teams
        mapOfParticipants.set(message.author.id,message.guild.member(message.author).displayName);

        /*mapOfParticipants.set("bonk", "bonk");
        mapOfParticipants.set("bonky", "bonky");
        mapOfParticipants.set("honky", "bonky honky");
        mapOfParticipants.set("rasmus", "Rasmus Sejr");
        mapOfParticipants.set("sejre", "Rasmus Sejre");
        mapOfParticipants.set("TomTom", "TomTom");
        mapOfParticipants.set("Lolleren", "Lolleren");*/

        //Default number of teams
        let numberOfTeams = 2;

        //Used to keep track of who made the last reaction
        let lastReacted;
        let isCreator = false;

        //Help Description
        const helpString = "Press " + emojiJoin + " to join \nPress " + emojiLeave + " to leave \n"
        const creatorHelpString = "Press " + emojiIncrement + " to increment number of teams \n" + "Press " + emojiDecrement + " to decrement number of teams \n" + "Press " + emojiOk + " to create the teams \n" + "Press " + emojiStop + " to stop the team creation";


        const baseEmbed = new Discord.MessageEmbed()
            .setColor("#0099ff").setTitle("Create Randomized Teams")
            .setDescription("**Participants:** \n" + mapToString(mapOfParticipants))
            .addField("**Number of teams to create**", numberOfTeams)
            .addField("**Participant Help**",helpString,true)
            .addField("**Creator Help**", creatorHelpString, true)
            .setFooter("Teams creator: " + message.author.username, message.author.avatarURL());

        message.channel.send(baseEmbed).then(newMSG => {
            //let test = mapToString(mapOfParticipants);
            newMSG.react(emojiJoin);
            newMSG.react(emojiLeave);
            newMSG.react(emojiIncrement);
            newMSG.react(emojiDecrement);
            newMSG.react(emojiOk);
            newMSG.react(emojiStop)
                .then(msgReact => {
                const collector = msgReact.message.createReactionCollector(filter,{time});
                collector.on('collect', reaction => {
                    //Collecter switch for each emojii
                    switch (reaction.emoji.name){
                        case emojiJoin:
                            if(!mapOfParticipants.has(lastReacted.id)){
                               mapOfParticipants.set(lastReacted.id,message.guild.member(lastReacted).displayName);
                               msgReact.message.edit(baseEmbed.setDescription("**Participants:** \n" + mapToString(mapOfParticipants)));
                            }

                            break;
                        case emojiLeave:
                            if(mapOfParticipants.has(lastReacted.id)){
                                mapOfParticipants.delete(lastReacted.id)
                                msgReact.message.edit(baseEmbed.setDescription("**Participants:** \n" + mapToString(mapOfParticipants)));
                            }
                            break
                        case emojiIncrement:
                            if(!isCreator) return;
                            //Calculates max number of teams calculated to the current amount of participants
                            let maxNumberOfTeams = Math.floor(mapOfParticipants.size/2)
                            if(numberOfTeams < maxNumberOfTeams){
                                numberOfTeams++;
                                baseEmbed.fields[0].value = String(numberOfTeams);
                                msgReact.message.edit(baseEmbed);
                            }
                            break;
                        case emojiDecrement:
                            if(!isCreator) return;
                            if(numberOfTeams > 2){
                                numberOfTeams--;
                                baseEmbed.fields[0].value = String(numberOfTeams)
                                msgReact.message.edit(baseEmbed);
                            }
                            break;
                        case emojiOk:
                            if(!isCreator) return;
                                if(mapOfParticipants.size > 1){
                                    createRandomTeams(false);
                                } else{
                                    const notEnoughParticipantsEmbed = new Discord.MessageEmbed().setDescription("Trying to create teams alone, get some friends...").setColor("#ff0000");
                                    message.reply(notEnoughParticipantsEmbed).then(message => message.delete({timeout: 20000}));
                                }
                            break;
                        case emojiStop:
                            if(!isCreator) return;
                            collector.stop();
                            messageDeleted = true;
                            break;
                        default:
                            const embedErrorMsg = new Discord.MessageEmbed().setColor("#FF0000").setDescription("Sorry but an error occurred");
                            message.channel.send(embedErrorMsg).then(message => message.delete({timeout: 20000}));
                            break;
                    }
                    if(!messageDeleted){
                        removeReactionOfUser(reaction, lastReacted);
                    }
                });
                    collector.on('end', collected => {
                        const stopEmbed = new Discord.MessageEmbed().setColor("#ffff00").setDescription("Teams creation was stopped");
                        message.channel.send(stopEmbed).then(message => message.delete({timeout: 10000}));
                        msgReact.message.delete();
                        message.delete();
                    });
            });
        });


        function createRandomTeams(returnData){
            //Initialize Team creation
            const teamsEmbed = new Discord.MessageEmbed().setTitle("Teams").setColor("#ffff00");
            let arrayOfTeams = new Array(numberOfTeams);



            let arrayOfParticipants = Array.from(mapOfParticipants.values());
            let minAmountOnTeam = Math.floor(mapOfParticipants.size/numberOfTeams)


            //Create teams
            for (let i = 0; i < numberOfTeams; i++){
                let randomMembers = arrayOfParticipants.sort(() => Math.random() - Math.random()).slice(0, minAmountOnTeam)
                arrayOfParticipants = arrayOfParticipants.filter(x => !randomMembers.includes(x));
                arrayOfTeams[i] = randomMembers;
            }

            //Add left over participants
            for (let i = 0; i < numberOfTeams; i++){
                if(arrayOfParticipants.length >= 1){
                    let randomMember = arrayOfParticipants.sort(() => Math.random() - Math.random()).slice(0, 1)
                    arrayOfParticipants = arrayOfParticipants.filter(x => !randomMember.includes(x));
                    arrayOfTeams[i] = arrayOfTeams[i].concat(randomMember[0]);
                }
            }

            let hehehah = arrayOfTeams.forEach(x => mapOfParticipants.get(x));

            for(let i = 0; i < numberOfTeams; i++){
                teamsEmbed.addField("Team " + (i+1) + ":", arrayOfTeams[i].join("\n"), true)
            }

            //Help info for teamEmbed
            teamsEmbed.addField("**HELP**","Press " + emojiShuffle +" to shuffle\n" + "Press " + emojiStop +" to stop")

            //Hacky solution for remake teams
            if(returnData){
                return teamsEmbed;
            }

            //Send and handle random teams
            let messageTeamsDeleted = false;
            message.channel.send(teamsEmbed).then(newMSG =>{
                newMSG.react(emojiShuffle);
                newMSG.react(emojiStop)
                    .then(msgReacts => {
                    const collector = msgReacts.message.createReactionCollector(filter,{time});
                    collector.on('collect',reaction => {
                       switch (reaction.emoji.name){
                           case emojiShuffle:
                               if(!isCreator) return;
                               msgReacts.message.edit(createRandomTeams(true));
                               break;
                           case emojiStop:
                               if(!isCreator) return;
                               messageTeamsDeleted = true
                               newMSG.delete();
                               break;
                           default:
                               console.log(reaction.emoji.name);
                               console.log(emojiShuffle);
                               break;
                       }
                        if(!messageTeamsDeleted){
                            removeReactionOfUser(reaction, lastReacted);
                        }
                    });
                });
            });
        }


        // check if the emoji is inside the list of emojis, if the user is not a bot, and if the reactor is the person who typed help
        function filter(reaction, user){
            //Check if the reactor is a bot
            if(user.bot){
                return false;
            }
            if((creatorReactions.includes(reaction.emoji.name)) && (user.id === message.author.id)){
                lastReacted = user;
                isCreator = true;
                return true;
            }
            lastReacted = user;
            return (!user.bot) && (participantsReactions.includes(reaction.emoji.name));
        }

        //Used to convert map to string with new line after each value in map
        function mapToString(map){
            let returnString = ""
            if(map.size > 0){
                map.forEach(function(value, key) {
                    returnString = returnString + value + "\n";
                })
            }
            return returnString;
        }

        function removeReactionOfUser(reaction, user){
            reaction.users.remove(user);
        }

    }
}