const Discord = require('discord.js');

module.exports = {
    name: 'resume',
    description: "Resume playing if the bot is paused and something is queued ",
    indepth:"",
    usage: "Just type !resume",
    execute(message, args, distube){
        var inChannel = message.member.voice.channel;

        if (!inChannel){
            const Embed = new Discord.MessageEmbed().setDescription("You must be in a channel, to play the bot").setColor("#ff0000")
            message.channel.send(Embed);
            return;
        }

        if(distube.isPaused(message)){
            message.react("👍");
            distube.resume(message);
        }

    }
}