const Discord = require('discord.js');

module.exports = {
    name: 'filter',
    description: "Changes the filter for the bot player",
    indepth:"Changes the filter for the bot player. Currently supports the following: `3d`, `bassboost`, `echo`, `karaoke`, `nightcore`, `vaporwave`, 'off'",
    usage: "Just type !filter **which filter**",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to change the filter").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                if ([`3d`, `bassboost`, `echo`, `karaoke`, `nightcore`, `vaporwave`].includes(args[0])){
                    let filter = distube.setFilter(message, args[0]);
                    const Embed = new Discord.MessageEmbed().setDescription("Set current filter to " + (filter || "Off"));
                    message.channel.send(Embed);
                }else {
                    const EmbedUnknownFilter = new Discord.MessageEmbed().setDescription("Sorry but i don't know that filter").setColor('#ff0000');
                    message.channel.send(EmbedUnknownFilter);
                }
                message.react(emoji);
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}