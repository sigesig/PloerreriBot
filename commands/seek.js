const Discord = require('discord.js');

module.exports = {
    name: 'seek',
    description: "Makes the bot go to a specific point in the playing song",
    indepth:"",
    usage: "Just type !seek",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                if(distube.isPlaying(message)) {
                    if(args[0].length > 0){
                        distube.seek(message, Number(args[0]));
                    } else {
                        const noArgEmbed = new Discord.MessageEmbed().setDescription("I need a point to jump to in the song").setColor("#ff0000");
                        message.channel.send(noArgEmbed);
                    }
                }else {
                    const notPlayingEmbed = new Discord.MessageEmbed().setDescription("I'm currently not playing anything").setColor("#ff0000");
                    message.channel.send(notPlayingEmbed);
                }
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}