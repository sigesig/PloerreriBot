const Discord = require('discord.js');

module.exports = {
    name: 'volume',
    description: "Changes the volume at which the bot plays",
    indepth:"Changes the volume at which the bot plays, from a scale of 0% to 100%",
    usage: "Just type !volume **amount**",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to change the volume").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                if (!distube.isPlaying(message)){
                    EmbedError.setDescription("The bot isn't currently playing anything")
                    message.channel.send(EmbedError);
                }
                else if (!args[0]){
                    EmbedError.setDescription("Please give a volume amount it can be from 0 to 100")
                    message.channel.send(EmbedError);
                }
                else if (!Number.isInteger(parseInt(args[0]))){
                    EmbedError.setDescription("Ey Einstein, Please give a volume amount it can be from 0 to 100")
                    message.channel.send(EmbedError);
                }
                else {
                    distube.setVolume(message, parseInt(args[0]));
                    const Embed = new Discord.MessageEmbed().setDescription("Changed the volume to `" + args[0] + "`").setColor("#FFFF00");
                    message.react(emoji);
                    message.channel.send(Embed);
                }

            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}