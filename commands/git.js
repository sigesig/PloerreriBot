const Discord = require('discord.js');

module.exports = {
    name: 'git',
    description: "This command is to get the git repository for the bot",
    indepth:"",
    usage: "Just type !git",
    execute(message, args){
        const Embed = new Discord.MessageEmbed().setDescription('https://gitlab.com/sigesig/PloerreriBot').setColor("#0099FF").setTitle("Git repository for bot");
        message.channel.send(Embed);
    }
}