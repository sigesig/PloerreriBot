const Discord = require('discord.js');

module.exports = {
    name: 'join',
    description: "Makes the bot join the channel you are in",
    indepth:"",
    usage: "Just type !join",
    execute(message, args){
        let emoji = '✌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in a channel to make the bot join").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            message.member.voice.channel.join();
            message.react(emoji);
        }

    }
}