const cheerio = require('cheerio')
const request = require('request')

module.exports = {
    name: 'dalle',
    description: "Snus Senpai",
    indepth:"",
    usage: "Just type !dalle",
    execute(message, args){

        var result = Math.floor(Math.random() * 2);
        if (result === 1){
            var options = {
                url: "http://results.dogpile.com/serp?qc=images&q=" + "snus",
                method: "GET",
                headers: {
                    "Accept": "text/html",
                    "User-Agent": "Chrome"
                }
            }
        }else {
            var options = {
                url: "http://results.dogpile.com/serp?qc=images&q=" + "weaboo",
                method: "GET",
                headers: {
                    "Accept": "text/html",
                    "User-Agent": "Chrome"
                }
            }
        }

        request(options, function (error, response, responseBody){
            if (error) return error;

            $ = cheerio.load(responseBody);

            var links = $(".image a.link");
            var urls = new Array(links.length).fill(0).map((v, i) => links.eq(i).attr("href"));

            //Check that we got some urls
            if (!urls.length){
                return;
            }
            //send the picture
            message.channel.send(urls[Math.floor(Math.random()*urls.length)]);
        });
    }
}