const Discord = require('discord.js');

module.exports = {
    name: 'pause',
    description: "Makes the bot pause the current playing song",
    indepth:"",
    usage: "Just type !pause",
    execute(message, args,distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to make it pause").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                distube.pause(message);
                message.react(emoji);
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}