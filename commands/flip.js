const Discord = require('discord.js');

module.exports = {
    name: 'flip',
    description: "Flip a coin",
    indepth:"",
    usage: "Just type !flip",
    execute(message, args){
        const Embed = new Discord.MessageEmbed().setTitle("Heads or Tails").setImage();
        var result = Math.floor(Math.random() * 2);
        if (result === 1){
            Embed.setDescription("**Heads**");
        }else {
            Embed.setDescription("**Tails**");
        }
        Embed.setColor("#0099ff")
        message.channel.send(Embed);
    }
}