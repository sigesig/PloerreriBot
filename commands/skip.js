const Discord = require('discord.js');

module.exports = {
    name: 'skip',
    description: "Makes the bot go to the skip",
    indepth:"",
    usage: "Just type !skip if you want to skip the current song. If you want to skip to a specific song in the queue type !skip **song queue number**",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to skip to the next song").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                if (args.length > 0) { //TODO: FIX able to skip to specific pos
                    return; //TODO: Make this
                } else {
                    if(distube.isPlaying(message)) {
                        console.log("Skipped")
                        distube.skip(message);
                    }else {
                        const notPlayingEmbed = new Discord.MessageEmbed().setDescription("I'm currently not playing anything").setColor("#ff0000");
                        message.channel.send(notPlayingEmbed);
                    }
                }
                message.react(emoji);
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}