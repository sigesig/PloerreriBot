const Discord = require('discord.js');
const helper = require('../misc/helperFunctions')

module.exports = {
    name: 'shuffle',
    description: "Shuffles the current queue",
    indepth:"",
    usage: "Just type !shuffle",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to shuffle the queue").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                const Embed = new Discord.MessageEmbed().setDescription("The current queue has been shuffled").setColor("#FFFF00");
                message.channel.send(Embed).then(newMessage => helper.messageDeleteAfterTime(newMessage,15000));
                message.react(emoji);
            } else {
                message.channel.send(EmbedError).then(newMessage => helper.messageDeleteAfterTime(newMessage,15000, message));;
            }

        }
    }
}