const Discord = require('discord.js');
const helper = require('../misc/helperFunctions')

module.exports = {
    name: 'loop',
    description: "Changes the bot player to loop/repeat the music",
    indepth:"Changes the bot player to loop/repeat the music. Depending on what you type after the !loop or !repeat. Type 0 to stop looping, 1 to loop current song, 2 to loop current queue.",
    usage: "Just type !loop **loop mode** or !repeat **loop mode**",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to change if the bot loop/repeat").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                let mode = distube.setRepeatMode(message, parseInt(args[0]));
                mode = mode ? mode === 2 ? "Repeat queue" : "Repeat song" : "Off";
                const Embed = new Discord.MessageEmbed().setDescription("Set repeat mode to `" + mode + "`").setColor("#FFFF00");
                message.channel.send(Embed).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000));
                message.react(emoji);
            } else {
                message.channel.send(EmbedError).then(newMessage => helper.messageDeleteAfterTime(newMessage,20000, message));
            }

        }
    }
}