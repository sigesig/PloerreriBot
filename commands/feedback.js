const fs = require('fs');
const Discord = require("discord.js");
module.exports = {
    name: 'feedback',
    description: "Used to give feedback of the bot",
    indepth:"Used to give feedback for the bot. i.e. if something works good, or if you find a bug. The please describe in depth the steps that let to the bug",
    usage: "Just type !feedback **feedback**",
    execute(message, args){
        if(args.length < 1){
            const errorEmbed = new Discord.MessageEmbed().setDescription("You actually need to give some feedback :>").setColor("#FF0000")
            message.channel.send(errorEmbed);
            return;
        }
        const ideaEmbed = new Discord.MessageEmbed()
            .setTitle("Feedback")
            .setAuthor(message.author.username,message.author.avatarURL())
            .setTimestamp()
            .setDescription(args.join(' '),)
            .setColor("#0099ff")
            .setFooter(message.guild.name);
        message.client.users.cache.get('256807585804582912').send(ideaEmbed);
        const Embed = new Discord.MessageEmbed().setDescription("Your feedback has been received, Thanks!").setColor("#0099ff")
        message.channel.send(Embed);
    }
}