const Discord = require('discord.js');

module.exports = {
    name: 'next',
    description: "Makes the bot go to the next song",
    indepth:"",
    usage: "Just type !next",
    execute(message, args, distube){
        let emoji = '👌';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to skip to the next song").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                if(distube.isPlaying(message)) {
                    console.log("Skipped")
                    distube.skip(message);
                }else {
                    const notPlayingEmbed = new Discord.MessageEmbed().setDescription("I'm currently not playing anything").setColor("#ff0000");
                    message.channel.send(notPlayingEmbed);
                }
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}