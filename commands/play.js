const Discord = require('discord.js');
const spotify = require('../misc/spotify');
const helper = require('../misc/helperFunctions')

module.exports = {
    name: 'play',
    description: "Play used to request a song or resume playing",
    indepth:"",
    usage: "Just type !play **followed by the song**. The song can be a link or some search queue like the name + artis. Can also be called just by typing !p",
    execute(message, args, distube){
        var inChannel = message.member.voice.channel;
        //general checks for the bot
        if(distube.isPaused(message)){
            message.react("👍");
            distube.resume(message);
            return;
        }
        if (!inChannel){
            const Embed = new Discord.MessageEmbed().setDescription("You must be in a channel, to play the bot").setColor("#ff0000")
            message.channel.send(Embed);
            return;
        }
        if (!args[0]){
            const Embed = new Discord.MessageEmbed().setDescription("You need to provide a song").setColor("#ff0000")
            message.channel.send(Embed);
            return;
        }
        
        
        //Check if it is a Spotify link to a playlist
        if(args.join().includes("open.spotify.com/")){
            spotify.playSpotify(args[0], message, distube);
        } else {
            //The following lines are to switch between choosing the first and best song fitting the search description, or getting 10 different choices.

            const isURL = helper.validURL(args.join());

            const firstSong = true
            if(firstSong && !isURL){
                distube.search(args.join()).then(searchResult => {
                    distube.play(message, searchResult[0].url);
                })

            } else {
                distube.play(message, args.join());
            }
        }



    }
}