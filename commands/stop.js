const Discord = require('discord.js');

module.exports = {
    name: 'stop',
    description: "Makes the bot stop/pause the current playing song",
    indepth:"",
    usage: "Just type !stop",
    execute(message, args,distube){
        let emoji = '🛑';
        const EmbedError = new Discord.MessageEmbed().setDescription("You must be in the same channel as the bot to make it stop").setColor('#ff0000');
        var inChannel = message.member.voice.channel;
        if (!inChannel){
            message.channel.send(EmbedError);
        }else {
            if (inChannel === message.guild.voice.channel) {
                if (distube.isPlaying(message)){
                    distube.stop(message);
                    message.react(emoji);
                }
            } else {
                message.channel.send(EmbedError);
            }

        }
    }
}