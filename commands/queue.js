const Discord = require('discord.js');
const helper = require('../misc/helperFunctions')

module.exports = {
    name: 'queue',
    description: "Prints the current queue ",
    indepth:"",
    usage: "Just type !queue",
    execute(message, args, distube){

        var inChannel = message.member.voice.channel;

        if (!inChannel){
            const Embed = new Discord.MessageEmbed().setDescription("You must be in a channel, to play the bot").setColor("#ff0000")
            message.channel.send(Embed);
            return;
        }
        let queue = distube.getQueue(message);
        if (queue){
            let prettyQueueText = queue.songs.map((song, id) => `**${id+1}**. [${song.name}] - \`${song.formattedDuration}\` - ${song.user}`);
            console.log(typeof prettyQueueText);
            const queueEmbed = new Discord.MessageEmbed().setTitle("Current queue:").setDescription(prettyQueueText).setColor("#ffff00");
            message.channel.send(queueEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,32000, message));
        } else {
            const Embed = new Discord.MessageEmbed().setColor("#FFFF00").setDescription("There are no songs in the queue");
            message.channel.send(Embed).then(newMessage => helper.messageDeleteAfterTime(newMessage,32000, message));
        }



    }
}