const Discord = require('discord.js')
const auth = require('./auth.json');
const DisTube = require('distube');
const fs = require('fs');
const helper = require('./misc/helperFunctions')

//Prefix for commands that the bot should read
const prefix = "-"

//Fuck Standly
let banStandlyOn = false;

//Initializing the bot
const bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

// Create a new DisTube
const distube = new DisTube(bot, {
    searchSongs: true,
    leaveOnEmpty: true,
    emitNewSongOnly: true,
    highWaterMark: 1 << 25,
});

// Previous message + label of message
class MessageInformation{
    constructor(currentMessage, label){
        this.crMessage = currentMessage;
        this.label = label;
    }
    getMessageExists(){
        return this.crMessage.deleted;
    }
}

const LabelEnum = Object.freeze({
        CURRENT: "current",
    });


let prevMessage;



bot.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'))
for(const file of commandFiles){
    const command = require(`./commands/${file}`);

    bot.commands.set(command.name, command);
}

bot.once('ready', function (evt) {
    console.log("DiscordDak Bot is online!");
    distube.on("initQueue", queue => {
        queue.autoplay = false;
        queue.volume = 50;
    });
    bot.user.setActivity(prefix + 'help', { type: 'LISTENING' }).catch(console.error);
});



/*
//This should resolve the not auto leaving when the bot is alone
bot.setInterval(function(){
        console.log("HEHE im not working")
        console.log(bot.voice.connections.values().length)
        if(1 === bot.voice.connections.values().length){
            bot.fetchGuildPreview().guild.voice.channel.leave();
        }
        console.log("I dont want to go in that if statement")
    }, 10000
    )
*/

bot.on('guildMemberAdd', member => {
    //Might have fixed it with allows from devel
    console.log("someone joined");
    if(banStandlyOn){
        //Check if the person who join is standly
        console.log(member.id + "This guy just joined")
        //Standly ID = 259432691055722497
        //Alex ID = 271681667389718529
        if (member.id === "256151538236522498"){
            member.kick('Han fortjente det');
        }
    }
});
bot.on('guildMemberRemove', member => {
    if(banStandlyOn){
        //Check if the person who join is standly
        console.log(member.id + "This guy just got kicked")
        if (member.id === "271681667389718529"){
            const Embed = new Discord.MessageEmbed().setDescription("Auto ban af standly kan fjernes af enten Mark, Yousef og Per /n skriv !banstandly").setTitle("Fuck Standly :>").setColor("#ff0000");
            member.guild.channels.get("256054698954457088").send(Embed);
        }
    }
});

bot.on('message', message =>{
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    const Embed = new Discord.MessageEmbed();
    const args = message.content.slice(prefix.length).split(/ +/);
    //message.guild.member("271681667389718529").kick();
    const command = args.shift().toLowerCase();
   //dalle 256151538236522498
   //  if (message.author.id === "256151538236522498"){
   //      message.guild.member("256151538236522498").kick()
   //  }

    console.log("Saw command " + command + " | With args " + args);
    switch (command) {
        case 'ping':
            bot.commands.get('ping').execute(message, args);
            break;
        case 'wednesday':
            bot.commands.get('wednesday').execute(message, args, distube, bot);
            break;
        case 'bak':
            bot.commands.get('bak').execute(message, bot, distube);
            break;
        case 'druk':
            bot.commands.get('druk').execute(message, args);
            break;
        case 'help':
            bot.commands.get('help').execute(message, args, bot);
            break;
        case 'idea':
            bot.commands.get('idea').execute(message, args);
            break;
        case 'alexander':
            bot.commands.get('alexander').execute(message, args);
            break;
        case 'play':
        case 'p':
            bot.commands.get('play').execute(message, args, distube);
            break;
        case 'leave':
        case 'fuckoff':
            if (distube.isPlaying(message)) {
                distube.stop(message);//Hacky solution
            }
            bot.commands.get('leave').execute(message, args, distube);
            break;
        case 'feedback':
            bot.commands.get('feedback').execute(message, args);
            break;
        case 'stop':
            bot.commands.get('stop').execute(message, args, distube);
            break;
        case 'pause':
            bot.commands.get('pause').execute(message, args, distube);
            break;
        case 'resume':
        case 'start':
            bot.commands.get('resume').execute(message, args, distube);
            break;
        case 'skip':
        case 's':
            bot.commands.get('skip').execute(message, args, distube);
            if (args.length > 0){
                const EmbedError = new Discord.MessageEmbed();
                EmbedError.setDescription("Invalid song queue number.").setColor("#ff0000");
                distube.jump(message, parseInt(args[0]))
                    .catch(err => message.channel.send(EmbedError));
            }
            break;
        case 'next':
        case 'n':
            bot.commands.get('next').execute(message, args, distube);
            break;
        case 'hahavictor':
            bot.commands.get('hahavictor').execute(message, bot, distube);
            break;
        case 'queue':
        case 'q':
            //let queue = distube.getQueue(message);
            bot.commands.get('queue').execute(message, args, distube);
            break;
        case 'flip':
            bot.commands.get('flip').execute(message, args);
            break;
        case 'join':
            bot.commands.get('join').execute(message, args);
            break;
        case 'git':
            bot.commands.get('git').execute(message, args);
            break;
        case 'volume':
            bot.commands.get('volume').execute(message, args, distube);
            break;
        case 'loop':
        case 'repeat':
            bot.commands.get('loop').execute(message, args, distube);
            break;
        case 'shuffle':
            bot.commands.get('shuffle').execute(message, args, distube);
            break;
        case 'filter':
            bot.commands.get('filter').execute(message, args, distube);
            break;
        case 'autoplay':
            bot.commands.get('autoplay').execute(message, args, distube);
            break;
        case 'fuckingbot':
        case 'lorttilwow':
        case 'boostedilol':
            message.channel.send("Lasse Standly");
            break;
        case 'mark':
            bot.commands.get('mark').execute(message, distube, bot);
            break;
        case 'dalle':
            bot.commands.get('dalle').execute(message, distube);
            break;
        case 'yousef':
            bot.commands.get('yousef').execute(message, distube, bot);
            break;
        case 'standly':
            bot.commands.get('standly').execute(message, distube, bot);
            break;
        case 'banstandly':
            switch(message.member.id){
                case "263322848356073473":
                case "256807585804582912":
                case "829377520293904414":
                case "255797642947198976":
                    banStandlyOn = (banStandlyOn == true) ? false : true;
                    const standlyEmbed = new Discord.MessageEmbed().setTitle("Auto Ban Standly").setDescription("Auto ban Standly set to: " + banStandlyOn.toString()).setColor("#FFFF00");
                    message.channel.send(standlyEmbed)
                break;
                default:
                    const errorEmbed = new Discord.MessageEmbed().setDescription("You are not allowed to use this function. Only Per, Yousef and Victor can use it").setColor("#ff0000");
                    message.channel.send(errorEmbed)
            }

            break;
        case 'kjelle': //TODO: Not working yet
            if (message.author.id === "256807585804582912"){
                role = message.guild.roles.fetch("798595844571267082");
                message.member.roles.add(role);
            }else {
                const Embed = new Discord.MessageEmbed().setDescription("You do not have permission to do this").setColor("#ff0000")
                message.channel.send(Embed);
            }
            break;
        case 'seek':
            bot.commands.get('seek').execute(message, args, distube);
            console.log("I Did seeking")
            break;
        case 'jump':
            bot.commands.get('jump').execute(message, args, distube);
            break;
        case 'teams':
                bot.commands.get('teams').execute(message);
            break;
        case 'rundetrunte':
            bot.commands.get('rundetrunte').execute(message, args);
            break;
        case 'craften':
            bot.commands.get('craften').execute(message, args);
            break;
        default:
            Embed.setDescription("Sorry, but i dont know that command :>").setColor("#ff0000");
            message.channel.send(Embed).then(message => message.delete({timeout: 15000}));
    }
});

// Queue status template
const status = (queue) => `Volume: \`${queue.volume}%\` | Filter: \`${queue.filter || "Off"}\` | Loop: \`${queue.repeatMode ? queue.repeatMode == 2 ? "All Queue" : "This Song" : "Off"}\` | Autoplay: \`${queue.autoplay ? "On" : "Off"}\``;
// DisTube event listeners, more in the documentation page
distube
    .on("playSong", (message, queue, song) => {
        var add = `Playing \`${song.name}\` - \`${song.formattedDuration}\`\nRequested by: ${song.user}\n${status(queue)}`;
        const playSongEmbed = new Discord.MessageEmbed().setDescription(add).setColor("#FFFF00");
        const deleteTimer = (song.duration + 1) * 1000;
        message.channel.send(playSongEmbed).then(msg => {
            msg.delete({timeout:parseInt(deleteTimer.toString())});
            prevMessage = new MessageInformation(msg, "")
        });
    })
    .on("addSong", (message, queue, song) => {
        var add = `Added ${song.name} - \`${song.formattedDuration}\` to the queue by ${song.user}`;
        const addSongEmbed = new Discord.MessageEmbed().setDescription(add).setColor("#FFFF00");
        message.channel.send(addSongEmbed);
        }
    )
    .on("playList", (message, queue, playlist, song) => {
        var add = `Play \`${playlist.name}\` playlist (${playlist.songs.length} songs).\nRequested by: ${song.user}\nNow playing \`${song.name}\` - \`${song.formattedDuration}\`\n${status(queue)}`
        const playListEmbed = new Discord.MessageEmbed().setDescription(add).setColor("#FFFF00");
        const deleteTimer = (song.duration + 1) * 1000;
        message.channel.send(playListEmbed).then(msg => {
            console.log("in delete")
            msg.delete({timeout:parseInt(deleteTimer.toString())});

        });
    })
    .on("addList", (message, queue, playlist) => {
        var add = `Added \`${playlist.name}\` playlist (${playlist.songs.length} songs) to queue\n${status(queue)}`;
        const addListEmbed = new Discord.MessageEmbed().setDescription(add).setColor("#FFFF00");
        message.channel.send(addListEmbed);
    })
    .on("searchResult", (message, result) => {
        let i = 0;
        var result =`**Choose an option from below**\n${result.map(song => `**${++i}**. ${song.name} - \`${song.formattedDuration}\``).join("\n")}\n*Enter anything else or wait 60 seconds to cancel*`;
        const searchResultEmbed = new Discord.MessageEmbed().setDescription(result).setColor("#FFFF00");
        message.channel.send(searchResultEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,61000, message));
    })
    // DisTubeOptions.searchSongs = true
    .on("searchCancel", (message) => {
        const searchCancelEmbed = new Discord.MessageEmbed(`Searching canceled`).setColor("#ffff00");
        message.channel.send(searchCancelEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,10000, message));
    })
    .on("empty",(message) => {
        const emptyEmbed = new Discord.MessageEmbed().setColor("#ffff00").setDescription(`Channel is empty. Leaving the channel`);
        message.channel.send(emptyEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,61000, message));
        message.guild.voice.channel.leave();
    })
    .on("finish",(message) => {
        const emptyEmbed = new Discord.MessageEmbed().setColor("#ffff00").setDescription(`Queue is empty.`);
        message.channel.send(emptyEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,61000, message));
    })
    .on("error", (message, err) => {
        const errorEmbed = new Discord.MessageEmbed().setColor("#FF0000").setTitle("An error encountered: ").setDescription(err);
        message.channel.send(errorEmbed).then(newMessage => helper.messageDeleteAfterTime(newMessage,61000, message));
    });

//Basically starts the bot
bot.login(auth.token);